const express = require('express');

const mongoose = require("mongoose");

require('dotenv').config();

const cors = require("cors");

const userRoutes = require('./routes/userRoutes');

const app = express();

app.use(cors());

app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.use("/api/users", userRoutes);

mongoose.connect(process.env.DB_CONNECTION,{
	useNewUrlParser: true,
	useUnifiedTopology: true
})


mongoose.connection.once('open', () => console.log('Now Connected to MongoDB Atlas'));






app.listen(process.env.PORT, () => {
	console.log(`API is now online on port:${process.env.PORT}`)
})